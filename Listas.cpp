/*
Laboratorio 1 Algoritmos y Estructuras de datos.
Integrantes: Diego Nuñez - Sebastian Bustamnte - Renata Arcos.
Se agregan los metodos pedidos al código previamente establecido.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// Clase Estudiante
class Estudiante {
    private: 
        string nombre;
        int edad;

    public:
        Estudiante(string n, int e) {
            nombre = n;
            edad = e;
        }

        Estudiante() {
            nombre = "";
            edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


// Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
// (cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante {
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }

        // Se hace overloading del metodo setVecino. Este método es llamado cuando se elimina 
        // el ultimo nodo de la lista, ya que el nuevo ultimo nodo no deberia tener un vecino.
        void setVecino() {
            this->vecino = NULL;
        }
                
        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};


// Clase ListaEstudiantes
class ListaEstudiantes {
    private:
        NodoEstudiante *primero, *ultimo;

    public:
        ListaEstudiantes(){
            primero = ultimo = NULL;
        }

        void insertarNodo(NodoEstudiante *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }
        }
	
	// Se muestra en pantalla la informacion de cada uno de los estudiantes en la lista.
        void mostrarLista(){
            NodoEstudiante *recorrer;

            recorrer = primero;

            cout << "\n-------------------------------" << endl;
            cout << "\tLista estudiantes:" << endl;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"\n-- Nombre: " << estudiante->getNombre()  << endl;;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
           cout << "\n-------------------------------\n" << endl;
        }

        // NUEVOS METODOS PEDIDOS
	
	// Se busca a un estudiante por el nombre y se retorna true o false.
        bool buscarEstudiante(string nombre) {
            NodoEstudiante *recorrer;
            recorrer = primero;

            while (recorrer != NULL) {
                if (nombre == recorrer->getEstudiante()->getNombre()) {
                    return true;
                } else {
                    recorrer = recorrer->getVecino();
                }
            }

            return false;
        }
	
	// Se agrega a un estudiante en el primer lugar de la lista.
        void insertarPrimero(NodoEstudiante *nuevo) {
            if (primero == NULL) {
            	// Si en la lista no habian elementos, se agrega el estudiante
                primero = ultimo = nuevo;
            } else {
            	// Si ya habian elementos, entonces el nuevo tendra de vecino al que antes era primero
                nuevo->setVecino(primero);
                primero = nuevo;
            }
        }
	
	// Se elimina el ultimo elemento de la lista
        void quitarUltimo(){
            if (ultimo != NULL){
                NodoEstudiante *recorrer;
                recorrer = primero;

                while (recorrer!=NULL) {
                    // Esto se ejecuta cuando hay un solo elemento en la lista
                    if (primero == ultimo) {
                        primero = ultimo = NULL;
                        break;
                    } else {

                        if (recorrer->getVecino() == ultimo) {
                            // Esto nunca sera verdadero cuando solo hay un elemento en la lista
                            ultimo = recorrer;
                            ultimo->setVecino();
                            break;
                        }else{
                            recorrer = recorrer->getVecino();
                        }
                    
                    }
                }
            }else{
                cout << "\n\t- No hay nodos disponibles.\n" << endl;
            }
        }
	
	// Se elimina el primer elemento de la lista.
        void quitarPrimero() {
            if (primero != NULL) {
                NodoEstudiante *nodoEliminar;
                nodoEliminar = primero;
                // Si el vecino del primero es NULL, entonces la lista quedara vacia
                primero = primero->getVecino();
                if (primero == NULL)
                    ultimo = NULL;
                delete nodoEliminar;
            } else {
                cout << "\n\t- No hay nodos disponibles.\n" << endl;
            }
        }
	
	// Se muestra el nombre del primer estudiante
        void getPrimero() {
            if (primero != NULL)
                cout << "primero: " << primero->getEstudiante()->getNombre() << endl;
            else
                cout << "\n\t- No hay nodos disponibles.\n" << endl;
        }
	
	// Se muestra el nombre del ultimo estudiante
        void getUltimo() {
            if (ultimo != NULL)
                cout << "ultimo: " << ultimo->getEstudiante()->getNombre() << endl;
            else
                cout << "\n\t- No hay nodos disponibles.\n" << endl;
        }
};

// Menu para manejar las acciones disponibles
void menu() {
    cout << "===================================" << endl;
    cout << "\tMenu de acciones\n" << endl;
    cout << "< 1 > Insertar nuevo nodo al principio de la lista." << endl;
    cout << "< 2 > Insertar nuevo nodo al final de la lista." << endl;
    cout << "< 3 > Eliminar el primer nodo de la lista." << endl;
    cout << "< 4 > Eliminar el ultimo nodo de la lista." << endl;
    cout << "< 5 > Buscar a un estudiante por su nombre." << endl;
    cout << "< 6 > Mostrar lista." << endl;
    cout << "< 0 > Finalizar ejecucion del programa." << endl;
    cout << "\nIngrese una opcion: ";

}

int main(){  

    srand(time(0));
    string nombres[12] = {"Bastian", "Sebastian", "Diego", "Sofia", "Ana", "Maria", "Penelope", "Florencia", "Bryan", "Jeremias", "Joaquin", "Camila"};

    ListaEstudiantes lista;
    string input1, input2;
    int choice;
    int randomNumber;

    while (true) {

        randomNumber = 6 + rand()%70;

        // En cada iteracion del ciclo se crea un estudiante random con su nodo correspondiente,
        // que esta disponible en caso de que se decida añadir un nuevo alumno.
        Estudiante random(nombres[rand()%12], randomNumber);
        NodoEstudiante *nuevo = new NodoEstudiante(random);
        
        menu();
        cin >> input1;

        try {
            choice = stoi(input1); // stoi: string to int
        } 
        catch (std::invalid_argument) {
            cout << "\n\t- Por favor ingresa un numero.\n" << endl;
            continue;
        }
        
        switch (choice) {

            case 1: // Insertar nuevo nodo al principio de la lista.
                lista.insertarPrimero(nuevo);
                break;

            case 2: // Insertar nuevo nodo al final de la lista.
                lista.insertarNodo(nuevo);
                break;

            case 3: // Eliminar el primer nodo de la lista.
                lista.quitarPrimero();
                break;

            case 4: // Eliminar el ultimo nodo de la lista.
                lista.quitarUltimo();
                break;

            case 5: // Buscar a un estudiante por su nombre.
                cout << "\nIngresa el nombre que deseas buscar: ";
                cin >> input2;

                if (lista.buscarEstudiante(input2))
                    cout << "\n\t- El estudiante " << input2 << " si esta en la lista.\n" <<endl;
                else
                    cout << "\n\t- El estudiante " << input2 << " no esta en la lista.\n" <<endl;

                break;

            case 6: // Mostrar lista.
                lista.mostrarLista();
                break;

            case 0: // Finalizar ejecucion del programa.
                return 0;

            default:
                cout << "\n\t- Por favor ingresa una de las opciones enumeradas.\n" << endl;
                continue;
        }

        // Si no se decidio añadir un nodo en la iteracion actual, 
        // se libera el espacio en memoria creado con new en la linea 251.
        if (choice != 1 && choice != 2) {
            delete nuevo;
        }
    }

    return 0;
}
